FROM ubuntu:16.04
ENV DEBIAN_FRONTEND noninteractive
MAINTAINER V. Korol <v.korol@i-free.com>

#asterisk
RUN apt-get update -q && apt-get install -y \
    asterisk \
    asterisk-mp3 \
    asterisk-dev \
    asterisk-ooh323 \
    asterisk-config \
    asterisk-modules \
    asterisk-moh-opsound-gsm \
    asterisk-moh-opsound-wav \
    asterisk-moh-opsound-g722

#supervisor
RUN apt-get update -q && apt-get install -y \
    supervisor

WORKDIR /tmp

COPY  supervisor/supervisord.conf /etc/supervisor/
COPY  supervisor/application-conf/ /etc/supervisor/conf.d/
COPY  asterisk/* /etc/asterisk/
COPY  voice/voice_notifier /usr/local/sbin/voice_notifier
COPY  voice/call_from_text.py /usr/local/sbin/
COPY  asterisk-chown-script /usr/local/sbin/

RUN ["chmod", "+x", "/usr/local/sbin/asterisk-chown-script"]
RUN ["chmod", "+x", "/usr/local/sbin/call_from_text.py"]
RUN mkdir /var/log/voice-notifier/
RUN chown -R asterisk:asterisk /var/log/voice-notifier
RUN chown -R asterisk:asterisk /usr/local/sbin/*

RUN apt-get update -q && apt-get install -y \
    python3.4 \
    python3-pip

RUN pip3 install --upgrade \
    wheel \
    setuptools

#utils
RUN apt-get update -q && apt-get install -y \
    git \
    vim \
    tar \
    curl \
    wget \
    sudo \
    tree \
    patch \
    rsync \
    locales \
    apt-utils

#other
RUN apt-get update -q && apt-get install -y \
    automake \
    autoconf \
    libgomp1 \
    pkg-config \
    libltdl-dev \
    build-essential

#audio requirements
RUN apt-get update -q && apt-get install -y \
    ffmpeg \
    mpg123 \
    libav-tools \
    libavcodec-extra

RUN pip3 install --upgrade \
    gTTS \
    pydub \
#    ffmpeg \
#    ffprove \
    jinja2


RUN apt-get -y purge \
    build-essential \
    asterisk-dev \
    libltdl-dev \
    pkg-config \
    automake \
    autoconf \
    libtool \
    rsync \
    patch \
    curl && \
    apt-get clean && \
    cd / && \
    rm -rf /var/lib/apt/lists/* /tmp/*

ENV TZ=Europe/Moscow \
    LANG=en_US.UTF-8
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN locale-gen ru_RU.UTF-8 && locale-gen en_US.UTF-8 && \
    locale -a && \
    dpkg-reconfigure locales

WORKDIR /
RUN sed -i 's/# MAXFILES=/MAXFILES=/' /usr/sbin/safe_asterisk

CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/supervisord.conf"]
