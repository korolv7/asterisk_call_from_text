#!/usr/bin/env python3
import os
import re
import sys
import time
import string
import random
import logging
from gtts import gTTS
from pydub import AudioSegment


class Mp3_maker(object):

    def __init__(self, params):
        self.filename = params.get('name', self.rand_name())
        self.text = params.get('text', 'Empty text')
        self.path = params.get('path', '/tmp/txt/')
        self.result_name = params.get('result_name', self.filename + '.mp3')
        self.result_path = params.get('result_path', '/var/lib/asterisk/sounds/')

    
    def rand_name(self, size = 8, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size)) + '.mp3'


    def get_mp3(self):
        text = self.text
        name = self.filename
        path = self.result_path
        files = []
        for item in text:
            tmp_file = path + item + '.mp3'
            files.append(tmp_file)
            if not os.path.isfile(tmp_file):
                if re.match("[a-zA-Z]", item) and 'i-free' not in item:
                    gtts_en = gTTS(item, lang = 'en')
                    with open(tmp_file, 'wb') as f:
                        gtts_en.write_to_fp(f)
                        f.closed
                else:
                    gtts_ru = gTTS(item, lang = 'ru')
                    with open(tmp_file, 'wb') as f:
                        gtts_ru.write_to_fp(f)
                        f.closed
        logging.info('Temporary files created')
        logging.debug(files)
        return files 


    def glue_mp3(self, current_list):
        name = self.result_name  
        path = self.result_path
        audios = AudioSegment
        final_mp3 = 'lul'
        for word in current_list:
            sound = audios.from_mp3(word)
            if final_mp3 == 'lul':
                final_mp3 = sound
            else:
                final_mp3 += sound
        final_mp3 = final_mp3 + 15
        final_mp3.export(path + name)
        logging.info('Final mp3 was created')
        logging.debug(name)
        return 0
