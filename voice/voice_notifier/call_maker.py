#!/usr/bin/env python3
import os
import sys
import time
import logging
from glob import glob
from jinja2 import Template
from shutil import move

class Call_maker(object):


    def __init__(self, params):
        self.filename = params.get('name')
        self.numbers = params.get('numbers', '79111001457')
        self.call_dir = params.get('call_dir', '/tmp/txt/call_files/')
        self.result_path = params.get('result_path', '/var/spool/asterisk/outgoing/')


    def make_callfile(self):
        numbers = self.numbers
        filename = self.filename
        path = self.call_dir
        os.makedirs(path, exist_ok=True)
        for idx, n in enumerate(numbers):
            with open('/usr/local/sbin/voice_notifier/call.sample','r') as f:
                t = Template(f.read())
                f.closed
            contain = t.render(mp3name = filename, number = n)
            with open(path + filename + str(idx) + '.call','w') as f:
                f.write(contain)
                logging.debug(contain)
                f.closed
        logging.info('Call_files was created')
        return 0


    def preparation(self):
        filename = self.filename
        path = self.call_dir
        call_files_list = []
        ls_pattern = path + filename + '*'
        for ifile in sorted(glob(ls_pattern)):
            call_files_list.append(ifile)
        if len(call_files_list) > 1:
            self.escalation(call_files_list)
        else:
            self.make_call(call_files_list[0])
        return 0 
    

    def escalation(self, call_files_list):
        filename = self.filename
        done_dir = self.result_path[:-1] + '_done/'
        ls_pattern = done_dir + filename
        for idx, ifile in enumerate(call_files_list):
            self.make_call(ifile)
            call_status = done_dir + filename + str(idx) + '.call'
            while not os.path.isfile(call_status): time.sleep(.10)
            with open(call_status, 'r') as f:
                status = f.read()
                f.closed
            if ('Expired' or 'Failed') in status:
                continue
            else:
                break
        return 0


    def make_call(self, call_file):
        outgoing_dir = self.result_path
        move(call_file, outgoing_dir)
        logging.info('%s was moved' % call_file)
        return 0
