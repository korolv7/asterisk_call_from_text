#!/usr/bin/env python3
import re
import os
import sys
import time
import logging
from glob import glob
from voice_notifier.mp3_maker import Mp3_maker
from voice_notifier.call_maker import Call_maker

WORKDIR = '/tmp/txt/'
LOGDIR = '/var/log/voice-notifier/'
TMPDIR = '/var/spool/asterisk/tmp/call_files/'
CURTIME = int(time.time())
STATUS_FAILED = -1


"""setup logging"""
os.makedirs(LOGDIR, exist_ok=True)
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s [%(levelname)s] %(module)s %(funcName)s %(message)s',
    filename = LOGDIR + sys.argv[0].split('/')[-1][:-3] +
    '.log.' + time.strftime("%Y-%m-%d", time.localtime())
)


def get_params(filename):
    try:
        with open(os.path.join(WORKDIR, filename + '.txt'), 'r') as f:
            notify_string = f.read()
            notify_string = re.sub(r"[^A-Za-zА-Яа-я]+", ' ', notify_string).split()
            params = {'name': filename,
                      'text': notify_string,
                      'path': WORKDIR,
                      'result_name': filename + '.mp3',
                      'result_path': '/var/lib/asterisk/sounds/'
                     }
            logging.info('Parameters were obtained')
            logging.debug(params)
            return params
    except Exception as e:
        logging.error(e)
        sys.exit(STATUS_FAILED)


def get_call_params(filename):
    try:
        with open(os.path.join(WORKDIR, filename + '.numbers'), 'r') as f:
            numbers = f.read()
            numbers = re.sub(r"\+", '', numbers).split()
            call_params = {'name': filename,
                           'numbers': numbers,
                           'call_dir': TMPDIR,
                           'result_path': '/var/spool/asterisk/outgoing/'
                          }
            logging.info('Call parameters were obtained')
            logging.debug(call_params)
            return call_params
    except Exception as e:
        logging.error(e)
        sys.exit(STATUS_FAILED)


def clean_done(done_dir, filename):
    for trash in (glob(done_dir + filename + '*')):
        os.remove(trash)
        logging.info('%s was removed' % trash)
        logging.info('Finished')
    return 0


if __name__ == '__main__':
    logging.info('Started')
    try:
        params = get_params(sys.argv[1])
        mp3_make = Mp3_maker(params)
        current_notify = mp3_make.get_mp3()
        mp3_make.glue_mp3(current_notify)
        call_params = get_call_params(sys.argv[1]) 
        call_make = Call_maker(call_params)
        call_make.make_callfile()
        call_file = call_make.preparation()
        clean_done(call_params['result_path'][0:-1] + '_done/', call_params['name'])


    except Exception as e:
        logging.error(e)
        sys.exit(STATUS_FAILED)
