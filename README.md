# zvonilka 
Dockerfile and set of files for running Asterisk with Voice-Notifier

## What is it based on?

Generally this is based on:
* Ubuntu 16.04.5 LTS
* Asterisk 11.7.0
* pydub + gTTS

## Building it.
```bash
git clone git@gitlab.com:korolv7/asterisk_call_from_text.git
cd docker_asterisk_zvonilka
docker build -t v.korol/zvonilka_ubuntu .
```

## Running it.
```bash
docker run -d \
    --name asterisk_ubuntu_zvonilka \
    -v asterisk-txt:/tmp/txt/ \
    -v asterisk-conf:/etc/asterisk/ \
    v.korol/zvonilka_ubuntu
```

## Using it
```bash
echo 'Hello! Server: Servername Problem: Low free memory Warning' > /tmp/txt/Servername-210703-2018-09-20.txt
echo '79219876543' > /tmp/txt/Servername-210703-2018-09-20.numbers
call_from_text.py Servername-210703-2018-09-20
```
