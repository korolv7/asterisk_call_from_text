#!/usr/bin/env python
# -*- coding: utf-8 -*-
import psycopg2
import sys
import os
import logging
import logging.config
import time
import yaml
import subprocess
import re


logging_config_path = '/etc/zabby/zabbix_caller_logging.ini'
if os.getenv('external_logging') == 'console':
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.config.fileConfig(logging_config_path)


STATUS_PROBLEM = -5
QUERY_TIMEOUT = 13
STATUS_TIMEOUT = -4
STATUS_EMPTY_RESULT = -6
PASSWORD_FILE = 'dbpass.yaml'
HOME_DIR = '/tmp/zabbix_caller/'
PERIOD = 1 * 24 * 60 * 60


def replace(string, old_pattern, new_pattern):
    while string.find(old_pattern) > -1:
        i = string.find(old_pattern)
        string = string[:i] + new_pattern + string[i + len(old_pattern):]
    return string

#TODO - USE DB
def translit(text):
    result = ''
    text = text.lower()

    letters = dict([('a', 'а'), ('b', 'б'), ('c', 'к'),
                    ('d', 'д'), ('e', 'е'), ('f', 'ф'),
                    ('g', 'г'), ('h', 'х'), ('i', 'и'),
                    ('j', 'дж'), ('k', 'к'), ('l', 'л'),
                    ('m', 'м'), ('n', 'н'), ('o', 'о'),
                    ('p', 'п'), ('q', 'кв'), ('r', 'р'),
                    ('s', 'с'), ('t', 'т'), ('u', 'у'),
                    ('v', 'в'), ('w', 'в'), ('x', 'икс'),
                    ('y', 'и'), ('z', 'з'), ('ё', 'йо')])

    symbols = dict([('.', ' '),
                    ('-', ' '),
                    ('_', ' ')])

    numbers = dict([('0', ' ноль '),
                    ('1', ' один '),
                    ('2', ' два '),
                    ('3', ' три '),
                    ('4', ' четыре '),
                    ('5', ' пять '),
                    ('6', ' шесть '),
                    ('7', ' семь '),
                    ('8', ' восемь '),
                    ('9', ' девять ')])

    pair_letters = dict([('zh', 'ж'), ('kh', 'х'), ('ts', 'ц'),
                         ('ch', 'ч'), ('yu', 'ю'), ('ya', 'я'),
                         ('ph', 'ф'), ('oo', 'у'), ('sh', 'ш')])

    words = dict([('i-free', 'ай фри'),
                  ('g01', 'джи ноль один'),
                  ('g02', 'джи ноль один'),
                  ('b2b', 'би ту би'),
                  ('zabbix', 'забикс'),
                  ('cm', 'цэ эм'),
                  ('hoster', 'хОстэр'),
                  ('lime', 'лайм'),
                  ('megafon', 'мегафон '),
                  ('viber', 'вайбер'),
                  ('interactive', 'интерактив'),
                  ('mts', 'эм тэ эс'),
                  ('messaging', 'мэссаджинг'),
                  ('db', 'дэ бэ'),
                  ('office', 'офис'),
                  ('eth', 'эз'),
                  ('subscriptions', 'сабскрипшонс'),
                  ('life', 'лайф'),
                  ('mysql', 'май эс кью эль'),
                  ('ext', 'экст'),
                  ('do-am', 'ду ам'),
                  ('cards', 'кардс'),
                  ('mobile', 'мобайл'),
                  ('backend', 'бэк энд'),
                  ('frontend', 'фронт энд'),
                  ('asc', 'а эс цэ'),
                  ('cdr', 'си ди эр'),
                  ('cache', 'кэш')])

    translit_rules = list([words, numbers, pair_letters,
                           symbols, letters])

    for dictionary in translit_rules:
        for key in dictionary:
            text = replace(text, key, dictionary[key])

    result = replace(text, '  ', ' ')
    return result


def convert_to_str(result):
    if len(result) > 1:
        res = []
        for i in range(len(result)):
            res.append(str(result[i][0]))
        return res
    elif (len(result) < 1):
        return 'None'
    else:
        return str(result[0][0])


def get_phone(user, cursor):
    query = 'select ldap_user_id_fk \
             from active_users \
             where id =' + str(user)
    ldap_user_id = convert_to_str(get_query_result(cursor, query))
    query = 'select contact_data \
             from ldap_contacts \
             where ldap_user_id_fk =' + ldap_user_id + \
            ' and contact_type=\'PHONE\''
    return convert_to_str(get_query_result(cursor, query))


def get_query_result(cursor, query):
    cursor.execute(query)
    pre_result = cursor.fetchall()
    result = []
    for i in range(len(pre_result)):
        result.append(list(pre_result[i]))
    return result


def rm_rows_by_host_regexp(itop_instructions, hostname):
    result = []
    for i in range(len(itop_instructions)):
        if (re.search(itop_instructions[i][2], hostname) is not None):
            result.append(itop_instructions[i])
    return result


def rm_rows_by_criticity(itop_instructions, criticity):
    result = []
    for i in range(len(itop_instructions)):
        if (itop_instructions[i][3] == str(criticity)):
            result.append(itop_instructions[i])
    return result


def rm_rows_by_priority(itop_instructions):
    result = []
    max_priority = 0
    for i in range(len(itop_instructions)):
        if (itop_instructions[i][5] > max_priority):
            max_priority = itop_instructions[i][5]
    for i in range(len(itop_instructions)):
        if (itop_instructions[i][5] == max_priority):
            result.append(itop_instructions[i])
    return result


def choose_instruction(itop_instructions, hostname, criticity):
    itop_instruction_id = 0
    itop_instructions = rm_rows_by_host_regexp(itop_instructions, hostname)
    if (len(itop_instructions) == 1):
        itop_instruction_id = itop_instructions[0][0]
        return itop_instruction_id
    else:
        itop_instructions = rm_rows_by_priority(itop_instructions)
        if (len(itop_instructions) != 1):
            itop_instructions = rm_rows_by_criticity(itop_instructions,
                                                     criticity)
        itop_instruction_id = itop_instructions[0][0]
        return itop_instruction_id


def get_itop_instruction_id(trigger_id, cursor):
    query = 'select trigger_template_id_fk \
             from zbx_triggers \
             where id = ' + str(trigger_id)
    trigger_template_id_fk = convert_to_str(get_query_result(cursor, query))
    query = 'select * from \
             itop_instructions \
             where trigger_template_id_fk = ' + str(trigger_template_id_fk)
    itop_instructions = get_query_result(cursor, query)
    query = 'select name from zbx_hosts \
             where id in (SELECT host_id_fk \
             from zbx_triggers where id = ' + str(trigger_id) + ')'
    host_name = convert_to_str(get_query_result(cursor, query))
    query = 'select criticity from itop_hosts \
             where host_id_fk in (select host_id_fk \
             from zbx_triggers where id = ' + str(trigger_id) + ')'
    criticity = convert_to_str(get_query_result(cursor, query))
    itop_instruction_id = choose_instruction(itop_instructions,
                                             host_name,
                                             criticity)
    return itop_instruction_id


def create_numbers_file(trigger_id, cursor):
    numbers = []
    try:
        query = 'select actual_instruction_id_fk \
                 from zbx_triggers \
                 where id=' + str(trigger_id)
        instruction_id = convert_to_str(get_query_result(cursor, query))
        try:
            if (instruction_id != 'None'):
                query = 'select active_user_id_fk \
                         from zbx_instruction_users \
                         where instruction_id_fk = ' + str(instruction_id)
            else:
                itop_instruction_id = get_itop_instruction_id(trigger_id,
                                                              cursor)
                query = 'select user_id_fk \
                         from itop_instruction_primary_users \
                         where itop_instruction_id_fk = ' + str(itop_instruction_id)
            result = convert_to_str(get_query_result(cursor, query))
            user_id = []
            if isinstance(result, list):
                user_id = result
            else:
                user_id.append(result)

            for i in range(len(user_id)):
                phone = get_phone(user_id[i], cursor)
                if (phone not in numbers):
                    numbers.append(phone)
        except Exception as err:
            logging.exception('The instruction has no responsible person ')
            logging.exception(err)
            sys.exit
    except Exception as err:
        logging.exception('There is no trigger with such id')
        logging.exception(err)
        sys.exit
    try:
        with open(HOME_DIR + str(trigger_id) + '.numbers', 'w') as file:
            file.write('7**********\n')
            file.write('7**********')
    except Exception as err:
        logging.exception('Can\'t write .numbers file')
        logging.exception(err)
        sys.exit


def create_txt_file(trigger_id, cursor):
    try:
        query = 'select manual_description \
                 from zbx_triggers \
                 where id=' + str(trigger_id)
        manual_description = convert_to_str(get_query_result(cursor, query))
        if (manual_description == 'None'):
            query = 'select trigger_template_id_fk \
                     from zbx_triggers \
                     where id=' + str(trigger_id)
            trigger_template = convert_to_str(get_query_result(cursor, query))
            query = 'select manual_description \
                     from zbx_trigger_templates \
                     where id=' + str(trigger_template)
            manual_description = translit(convert_to_str(get_query_result(cursor, query)))
        else:
            manual_description = translit(convert_to_str(manual_description))
    except Exception as err:
        logging.exception('There is some problems with \
                           manual description for trigger')
        logging.exception(err)
        sys.exit
    query = 'select name from zbx_hosts \
             where id in (SELECT host_id_fk \
             from zbx_triggers where id = ' + str(trigger_id) + ')'
    hostname = convert_to_str(get_query_result(cursor, query))
    host = translit(hostname)

    text = 'Вас приветствует техническая поддержка группы компаний Ай Фри\n'
    text += 'Сервер ' + host + '\n'
    text += manual_description
    text += '     Повторяю     '
    text += 'Сервер ' + host + '\n'
    text += manual_description
    try:
        with open(HOME_DIR + str(trigger_id) + '.txt', 'w') as file:
            file.write(text)
    except Exception as err:
        logging.exception('Can\'t write .txt file')
        logging.exception(err)
        sys.exit


def get_file_age(filename):
    return time.time() - os.path.getmtime(filename)


def check_file_existance(filename):
    return os.path.exists(filename) and get_file_age(filename) < PERIOD


if __name__ == '__main__':
    trigger_id = sys.argv[1]
    try:
        with open(PASSWORD_FILE, 'r') as file_pointer:
            config = yaml.load(file_pointer)
            connection_parameters = config['zabbix_instructions']
    except Exception as err:
        logging.exception('Can\'t read PASSWORD_FILE')
        logging.exception(err)
        sys.exit
    try:
        conn = psycopg2.connect(**connection_parameters)
        cursor = conn.cursor()

        file_name = HOME_DIR + trigger_id
        if check_file_existance(file_name + '.txt'):
            logging.debug('File is already exists')
        else:
            create_txt_file(trigger_id, cursor)

        if check_file_existance(file_name + '.numbers'):
            logging.debug('File is already exists')
        else:
            create_numbers_file(trigger_id, cursor)
        conn.close()
    except psycopg2.extensions.QueryCanceledError as err:
        logging.exception(err)
        print(STATUS_TIMEOUT)
        sys.exit(STATUS_TIMEOUT)
    except Exception as err:
        logging.exception(err)
        print(STATUS_PROBLEM)
        sys.exit(STATUS_PROBLEM)
    filename = str(trigger_id)
    numberfile = '/tmp/zabbix_caller/' + filename + '.numbers'
    textfile = '/tmp/zabbix_caller/' + filename + '.txt'
    subprocess.call(['scp', '-i', '/etc/zabbix/.zabby/id_rsa',
                     numberfile, 'root@docker:\
                     /var/lib/docker/volumes/asterisk-txt/_data/'])
    subprocess.call(['scp', '-i', 'id_rsa',
                     textfile, 'root@docker:\
                     /var/lib/docker/volumes/asterisk-txt/_data/'])
    subprocess.call(['ssh', '-i', 'id_rsa',
                     'root@docker', 'docker',
                     'container', 'exec', 'asterisk_ubuntu_zvonilka_16',
                     'sudo', '-u', 'asterisk', 'call_from_text.py', filename])

